import matplotlib.pyplot as plt 
import numpy as np 

def activation_function(a, threshold = 0.5):
    if(a < threshold):
        return 0
    else:
        return 1

#hebbian learning rule
def hebbian(X, Y, W, b, iterations = 100, show = 1):
    a = np.zeros(len(Y))
    h = np.zeros(len(Y))
    error = np.zeros(iterations)
    it = np.zeros(iterations)
    alpha = 0.1
    
    for k in range(iterations):
        for i in range(len(Y)):
            a[i] = b 
            for j in range(len(W)): 
                a[i] +=  X[j][i] * W[j] 
            h[i] = activation_function(a[i], 0.5)

            if(h[i] != Y[i]):
                for j in range(len(W)):
                    W[j] = W[j] + alpha* Y[i] * X[j][i]
                b = b + alpha * Y[i]

        error[k] = 0
        for i in range(len(Y)):
            error[k] += ( Y[i] - h[i] )**2
        it[k] = k+1

    if(show ==1):
        '''
        plt.plot(it, error)  
        plt.xlabel('Iterations')
        plt.ylabel('Mean Square Error')
        plt.show()  '''   

        #s = s + 'Iterations = '+ str(iterations) + '\n'
        #for j in range(len(W)):
        #    s = s + 'W[' + str(j) + '] = ' + str(W[j]) + '\n'
        #s = s + 'b = ' + str(b) + '\n'     

    return h


def printTruthTable(gate,X,Y):   
    s = gate  + ' Truth Table : \n'
    s = s + 'Inputs\tOutput\n'
    for i in range(len(Y)):
        for j in range(len(X)):
            s = s + str(int(round(X[j][i]))) + ' \t'
        s = s + str(int(round(Y[i]))) + '\n'

    print(s)




# input dataset
X = [ [0,0,1,1], [0,1,0,1] ]
X_tmp = [[0,1,0,1], [0,0,1,1] ]
Y_or = [0, 1, 1, 1]
Y_and = [0, 0, 0, 1]
Y_andnot = [0, 0, 1, 0]
Y_xor = [0, 1, 1, 0]
W = [0.5, 0.5]
b = -2


Yn = hebbian([[0,1]],[1,0],[-1],b)
printTruthTable('NOT gate',[[0,1]],Yn)

Yand = hebbian(X,Y_and,W,b)
printTruthTable('AND gate',X,Yand)

Yor = hebbian(X,Y_or,W,b)
printTruthTable('OR gate',X,Yor)

Yandn = hebbian(X,Y_andnot,[2, -2],b)
printTruthTable('AND NOT gate',X,Yandn)


#NAND
Y4= hebbian(X,[1, 1, 1, 0],[-1,-1],1)
printTruthTable('NAND gate',X,Y4)

#NOR
Y6= hebbian(X,[1, 0, 0, 0],[-1,-1],b)
printTruthTable('NOR gate',X,Y6) 

#XOR
Y1 = hebbian(X,Y_andnot,[2, -2],-1,100,0)
Y2 = hebbian(X_tmp,[0, 1, 0, 0],[2, -2],-1,100,0)
Yxor = hebbian([Y1,Y2], Y_xor,W,b)
printTruthTable('XOR gate',X,Yxor)
