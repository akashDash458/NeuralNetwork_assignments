import numpy as np

def perceptron(weights, inputs, bias):
    x = np.add(np.dot(inputs, weights), bias)
    logit = activation_function(x, type="sigmoid")
    return np.round(logit)


def activation_function(x, type="sigmoid"):
    return  1 / (1 + np.exp(-x))
    


def compute(data, logic_gate, weights, bias):
    weights = np.array(weights)
    output = np.array([ perceptron(weights, datum, bias) for datum in data ])

    return output


def print_template(dataset_table, name, data):
    # act = name[6:]
    print("Logic Function: {}".format(name.upper()))
    print("X0\tX1\tY")
    toPrint = ["{1}\t{2}\t{3}\t{0}".format(output, *datas) for datas, output in zip(dataset_table, data)]
    for i in toPrint:
        print(i)


def main():
    dataset_table = np.array([[0, 0],[0, 1],[1, 0],[1, 1]])
    gates = {
        "and": compute(dataset_table, "and", [1, 1, 1], -2),
        "or": compute(dataset_table, "or", [1, 1, 1], -0.9),
        "not": compute(np.array([ [0], [1] ]), "not", [-1], 1),
        "nand": compute(dataset_table, "nand", [-1, -1, -1], 3),
        "nor": compute(dataset_table, "nor", [-1, -1, -1], 1),
        # _xor = compute(dataset_table, "and", [1], dataset_table),
        # _xnor = compute(dataset_table, "xnor", [], dataset_table)
    }

    for gate in gates:
        print_template(dataset_table, gate, gates[gate])