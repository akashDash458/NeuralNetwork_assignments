# Imports
import numpy as np 
      
# Each row is a training example, each column is a feature  [X1, X2, X3]
X=np.array(([0,0,1],[0,1,1],[1,0,1],[1,1,1]), dtype=float)
y=np.array(([0],[1],[1],[0]), dtype=float)

# Define useful functions    

# Activation function
def sigmoid(x):
    return 1/(1+np.exp(-x))

# Derivative of sigmoid
def sigmoid_derivative(x):
    return x * (1 - x)

# Class definition
class NeuralNetwork:
    def __init__(self, x,y):
        self.input = x
        self.weights1= np.random.rand(self.input.shape[1],4) # considering we have 4 nodes in the hidden layer
        print(self.weights1.shape)
        self.weights2 = np.random.rand(4,1)
        print(self.weights2.shape)
        self.y = y
        self.layer1 = np.zeros((4,4))
        self.layer2 = np.zeros((4,1))
        self.output = np. zeros(y.shape)
        
    
    def feedforward(self):
        #self.layer1 = sigmoid(np.dot(self.input, self.weights1))
        #print(self.layer1.shape)
        
        for i in range(len(self.input)):
            for j in range(len(self.weights1[0])):
                A = self.input[i]
                B = [item[j] for item in self.weights1]
                self.layer1[i][j] = sigmoid(np.dot(A,B))
        
        
        
        
        #self.layer2 = sigmoid(np.dot(self.layer1, self.weights2))
        #print(self.layer2.shape)
        
        for i in range(len(self.layer1)):
            for j in range(len(self.weights2[0])):
                A = self.layer1[i]
                B = [item[j] for item in self.weights2]                
                self.layer2[i][j] = sigmoid(np.dot(A,B))
        
        return self.layer2
    
    
    
    def backprop(self):        
               
        layer1_trans = np.transpose(self.layer1)
       
        
        for i in range(len(layer1_trans)):
            der = 2*sigmoid_derivative(self.output[i])*(self.y[i] - self.output[i])         
            
            self.weights2[i] += sum(der* x for x in layer1_trans[i])
        
        
       
        
        #d_weights1 = np.dot(self.input.T, np.dot(2*(self.y -self.output)*sigmoid_derivative(self.output), self.weights2.T)*sigmoid_derivative(self.layer1))
        #input_trans = np.transpose(self.input)
        #w2_trans = np.transpose(self.weights2)
        '''
        
        for i in range(len(y)):
            A = 
            d_weights1_t[i] = [x* A for x in self.input[i]
        '''
       
        d_wt1_t =np.zeros((4,3))
        
        der1 = np.zeros((4,4))
        
        for i in range(len(y)):
            A = 2* (self.y[i] - self.output[i])* self.weights2[i]
            d_wt1_t[i] = [x* A for x in self.input[i]] 
        
               
        for i in range(len(self.layer1)):
            for j in range(len(self.layer1[i])):
                der1[i][j] = sigmoid_derivative(self.layer1[i][j])   
                
                    
        self.weights1 += np.matmul(np.transpose(d_wt1_t),der1)
        
       
        
    def train(self, X, y):
        self.output = self.feedforward()
        self.backprop()
        

NN = NeuralNetwork(X,y)
for i in range(1001): # trains the NN 1000 times
    if i % 100 ==0: 
        print ("for iteration # " + str(i) + "\n")
        print ("Input : \n" + str(X))
        print ("Actual Output: \n" + str(y))
        print ("Predicted Output: \n" + str(NN.feedforward()))
        print ("Loss: \n" + str(np.mean(np.square(y - NN.feedforward())))) # mean sum squared loss
        print ("\n")
  
    NN.train(X, y)