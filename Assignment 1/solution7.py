# example of training a final classification model
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import train_test_split
import pandas as pd

data=pd.read_excel("../Dataset/data3.xlsx",header=None)
X = data.iloc[:,:-1]
Y = data.iloc[:,-1]
X_train,X_test,Y_train,Y_test = train_test_split(X,Y,test_size=0.4)

# generate 2d classification dataset
model=LogisticRegression()
# fit final model
model.fit(X_train,Y_train)
print('Accuracy', model.score(X_test,Y_test))

