#Implement the ridge regression problem by considering both batch gradient descent and
#stochastic gradient descent. (a) Plot the cost function vs the number of iterations for both
#the cases. (b) Plot the cost function (J) vs w1 and w2 in a contour or 3D surf graph for both
#the cases. (Please use the dataset “data.xlsx”). (Use for or while loop for the
#implementation)

#import the libraries
import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D

#declare learning rate and no. of iterations
learning_rate=0.0000000009
n = 1500
fig1=plt.figure(1)
fig2=plt.figure(2)
ax1= fig1.add_subplot(111)
ax1.set(xlabel='Iterations', ylabel='Cost', title='Ridge Regression')
ax2 = fig2.add_subplot(111, projection='3d')
ax2.set_xlabel('w1')
ax2.set_ylabel('w2')
ax2.set_zlabel('COST') 

#read from dataset
data= pd.read_excel("dataset/data.xlsx",header=None)

#store the data in different variables
#x= feature matrix  y= label vector
x=[data[0].tolist(), data[1].tolist()]
y=data[2].tolist()
#define the model coefficients
#w0=1, w1=0.1, w2=0.01
w=[1,0.1,0.01]

reg_parameter=0.0001
#compute
for i in range(n):
    #predict the current output
    hypothesis = w[0] +  np.multiply(w[1],x[0]) + np.multiply(w[2],x[1])
    for j in range(len(x[0])):
        w[0]=w[0]-learning_rate*(hypothesis[j]-y[j])           
        w[1]=w[1]-learning_rate*(hypothesis[j]-y[j])*x[0][j]
        w[2]=w[2]-learning_rate*(hypothesis[j]-y[j])*x[1][j]
    cost=np.square(np.subtract(hypothesis, y)).mean()
    
    #plot cost vs iteration
    ax1.plot(i,cost,'b,')     
    #plot cost vs weights 
    ax2.scatter(w[1],w[2],cost,c='r', marker=',')  

print(cost)    
plt.show()        
    
