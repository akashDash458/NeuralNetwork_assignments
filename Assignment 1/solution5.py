#import the libraries
import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd

#import the database
data=pd.read_excel("dataset/data.xlsx",header=None)

#store the data in different variables
x = data.iloc[:,0:2]
y=data[2].tolist()

#define the model coefficients
#w=np.random.rand(2)
#b=np.random.rand(1)
w=[0.1,0.01]
bias=1

#declare learning parameter
reg_parameter=1

#compute
x_trans=np.transpose(x)    
w=np.dot(np.linalg.inv(np.dot(x_trans,x)+reg_parameter*np.identity(x_trans.shape[0])),np.dot(x_trans,y))
pred=np.dot(x,w)
cost=np.square(np.subtract(pred, y)).mean()
print(cost)