#Implement the linear regression algorithm to estimate the weight parameters for
# the feature matrix (X) and the class label vector (y). 
# (a) Plot the cost function  vs the number of iterations. 
# (b) Plot the cost function (J) vs w1 and w2 in a contour or 3D surf graph (w=
#[w0 w1 w2]). Please use the dataset “data.xlsx”. (Use for or while loop for the
# implementation)

import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D

"""def linear_regression(x, y, w0, w1, w2, iterations, learning_rate):
    N = float(len(y))
    fig1=plt.figure(1)
    ax1= fig1.add_subplot(111)
    ax1.set(xlabel='Iterations', ylabel='Cost', title='Batch Gradient Descent')
    print("ENTERED")
    print(N)
    for i in range(iterations):
        estimate = w0 +  np.multiply(w1,x[0]) + np.multiply(w2,x[1])
        diff = y - estimate
        cost = sum([data**2 for data in (diff)]) / N
        grad0 = -(2/N) * sum(diff)
        grad1 = -(2/N) * sum(x[0] * diff)
        grad2 = -(2/N) * sum(x[1] * diff)
        w0 = w0 - (learning_rate * grad0)
        w1 = w1 - (learning_rate * grad1)
        w2 = w2 - (learning_rate * grad2)
        ax1.plot(i,cost,'b,')
        print(cost)
    return w0, w1, w2, cost """




#declare learning rate and no. of iterations


#declare two plots and axex properties
fig2=plt.figure(2)
ax2 = fig2.add_subplot(111, projection='3d')
ax2.set_xlabel('w1')
ax2.set_ylabel('w2')
ax2.set_zlabel('COST')  



#read from dataset
#x= feature matrix  y= label vector
data= pd.read_excel("dataset/data.xlsx",header=None)

#store the data in different variables
x=[data[0].tolist(), data[1].tolist()]
y=data[2].tolist()
#define the model coefficients
#w0=1
#w1=0.1
#w2=0.01
w = [1, 0.1, 0.01]
learning_rate=0.00000001
iterations = 1000
#w0, w1, w2, cost = linear_regression(x,y,w0,w1,w2,iterations,learning_rate)
n = len(y)


for i in range(n):
    #predict the current output
    hypothesis = w[0] +  np.multiply(w[1],x[0]) + np.multiply(w[2],x[1])
    cost=np.square(np.subtract(hypothesis, y)).mean()
    diff= hypothesis - y
    w[0] = w[0]-learning_rate*(diff)
    w[1] = w[1]-learning_rate*(diff)*x[0]
    w[2] = w[2]-learning_rate*(diff)*x[1]
   
    
    #plot cost vs iteration
    ax2.plot(i,cost,'b,')  
    
    #plot cost vs weights
    #ax2.scatter(w[1],w[2],cost,c='r', marker=',')

for i in range(100):
    for j in range(100):
        w1 = (i-50)/1000
        w2 = (j-50)/10000
        hypothesis = w0 +  np.multiply(w1,x[0]) + np.multiply(w2,x[1])
        cost=np.square(np.subtract(hypothesis, y)).mean()
        ax2.scatter(w1,w2,cost,c='r', marker=',')
       

print(cost)    
plt.show()    
    
