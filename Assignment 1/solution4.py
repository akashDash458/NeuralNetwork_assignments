#Implement the Vectorized linear regression problem to evaluate the weight parameters
#for question number 1. Compare the weight parameters with the weights obtained using
#both gradient descent and stochastic gradient descent based algorithms. (Please use the
#dataset “data.xlsx”).

import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd

#import the database
data= pd.read_excel("dataset/data.xlsx",header=None);

#store the data in different variables
x = data.iloc[:,0:2]
y=data[2].tolist()

w=[0.1,0.01]
bias=1
#declare learning parameter
learning_parameter=0.0000000009
reg_parameter=0.0001

#compute
x_trans=np.transpose(x)    
w=np.dot(np.linalg.inv(np.dot(x_trans,x)),np.dot(x_trans,y))
hypothesis=np.dot(x,w)
cost=np.square(np.subtract(hypothesis, y)).mean()
print(cost)